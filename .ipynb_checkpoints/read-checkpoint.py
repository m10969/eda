import pandas as pd
import json
from datetime import datetime

def fecha_to_datetime(fecha: str) -> datetime:
    return datetime.strptime(fecha, '%d/%m/%Y')

def dispositivo_to_dict(dispositivo: str) -> dict:
    return json.loads(dispositivo.replace('\'', '"').replace(';', ','))

def parse_raw_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    df.dispositivo = df.dispositivo.apply(lambda x: dispositivo_to_dict(x))
    df.fecha = df.fecha.apply(lambda x: fecha_to_datetime(x))
    df.monto = df.monto.str.replace(',', '.').astype(float)
    df.cashback = df.cashback.str.replace(',', '.').astype(float)
    df.dcto = df.dcto.str.replace(',', '.').astype(float)
    df.interes_tc = df.interes_tc.astype(float)
    
    df = df.join(pd.json_normalize(df.dispositivo)).drop(columns=['dispositivo'])
    
    
    df.to_feather('data.feather')
            
    return df